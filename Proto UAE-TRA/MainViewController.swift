//
//  ViewController.swift
//  Proto UAE-TRA
//
//  Created by Kirill Gorbushko on 10.08.15.
//  Copyright © 2015 ThinkMobiles. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {

    private struct Constant {
        static let CellIdentifier = "protoCell"
    }

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet var searchDisplayTableViewController: UISearchDisplayController!
    
    var dataSource: Array<String> = []
    var searchDataSource: Array<String> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        prepareDataSource()
        prepareSearchBar()
        prepareTableView()
    }

}

extension MainViewController : UITableViewDataSource {
    // MARK: - UITableViewDataSource
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == searchDisplayController?.searchResultsTableView {
            return searchDataSource.count
        } else {
            return dataSource.count
        }
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = self.tableView.dequeueReusableCellWithIdentifier(Constant.CellIdentifier, forIndexPath: indexPath)
        prepareCell(cell, atIndexPath: indexPath)
        
        return cell
    }
}

extension MainViewController: UITableViewDelegate {
    // MARK: - UITableViewDelegate
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
}

extension MainViewController : UISearchDisplayDelegate {
    // MARK: - UISearchDisplayDelegate
    func searchDisplayController(controller: UISearchDisplayController, shouldReloadTableForSearchString searchString: String?) -> Bool {
        searchDataSource.removeAll()
        if (searchString?.isEmpty != nil) {
            let predicate = NSPredicate(format: "SELF contains %@", searchString!)
            searchDataSource = dataSource.filter { predicate.evaluateWithObject($0) }
        }
        return searchDataSource.count > 0
    }
    
    func searchDisplayController(controller: UISearchDisplayController, willShowSearchResultsTableView tableView: UITableView) {
        searchDisplayController?.searchResultsTableView.separatorColor = UIColor.clearColor()
    }
    
    func searchDisplayControllerWillEndSearch(controller: UISearchDisplayController) {
        tableView.reloadSections(NSIndexSet(index: 0), withRowAnimation: UITableViewRowAnimation.Fade)
    }
}

extension MainViewController {
    // MARK: - Private
    private func prepareCell(cell:UITableViewCell, atIndexPath indexPath:NSIndexPath) {
        cell.textLabel?.text = dataSource[indexPath.row]
        cell.imageView?.image = UIImage(named: "info")
    }
    
    func prepareDataSource() {
        dataSource = [
            "Domain CheckService",
            "Compliant about service provider",
            "Compliant about TRA",
            "Enquires",
            "Suggestion",
            "SMS Spam",
            "Poor Coverage",
            "Help Salim"
        ]
    }
    
    func prepareSearchBar() {
        let imageWithColor = UIImage.imageWithColor(UIColor.searchBarColor())
        searchBar.setBackgroundImage(imageWithColor, forBarPosition: UIBarPosition.TopAttached, barMetrics: UIBarMetrics.Default)
        
        for subview in (searchBar.subviews.first?.subviews)! {
            if subview.isKindOfClass(UITextField.classForCoder()) {
                subview.backgroundColor = UIColor.whiteColor()
                (subview as! UITextField).tintColor = UIColor.searchBarColor()
                subview.alpha = 1.0
            }
        }
        
    }
    
    func prepareTableView() {
        tableView.tableFooterView = UIView(frame: CGRectZero)
    }
}


