//
//  UIColor+AppColors.swift
//  Proto UAE-TRA
//
//  Created by Kirill Gorbushko on 10.08.15.
//  Copyright © 2015 ThinkMobiles. All rights reserved.
//

import UIKit

extension UIColor {
    
    class func searchBarColor() -> UIColor {
        return colorWith(35, green: 180, blue: 178, alpha: 1)
    }
    
    private class func colorWith(r:Float, green g:Float, blue b:Float, alpha a:Float) -> UIColor {
        return UIColor(colorLiteralRed: r/255.0, green: g/255.0, blue: b/255.0, alpha: a)
    }
}