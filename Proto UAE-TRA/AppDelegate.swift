//
//  AppDelegate.swift
//  Proto UAE-TRA
//
//  Created by Kirill Gorbushko on 10.08.15.
//  Copyright © 2015 ThinkMobiles. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        return true
    }

}

